import axios from "axios"

const Axios = axios.create({
  baseURL : 'http://127.0.0.1:8000/api/',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    timeout : 1000
  }
})

Axios.interceptors.request.use((config) => {
  return config
})

Axios.interceptors.response.use((response) => {
  return response
})

export default Axios