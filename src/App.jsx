import './App.css'
import { Routes, Route } from 'react-router-dom'
import Home from 'pages/home'
import Product from 'pages/product'

const App = () => {
  return (
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route exact path="/product" element={<Product />} />
    </Routes>
  )
}

export default App
