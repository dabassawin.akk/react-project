import express from 'express'
import path from 'path'
import cors from 'cors'
import { fileURLToPath } from 'url'

const app = express()

const DIST_DIR = path.join(fileURLToPath(import.meta.url), '../../dist')
const HTML_FILE = path.join(DIST_DIR, 'index.html')
const PORT = 3000

app.disable("x-powered-by")

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  next()
})

app.get('/healthcheck', (req, res) => {
  res.json({ status: 'UP' })
})

app.use(cors())
app.use(express.static(DIST_DIR))

app.get('*', (req, res) => res.sendFile(HTML_FILE))

app.listen(PORT, (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log('> App is listening on port ' + PORT + '\n')
  }
})
