FROM node:18.17.0

ENV NODE_ENV=production

WORKDIR /app

COPY package.json .
RUN npm install --production

COPY . .
RUN npm run build

CMD ["node", "server/start.js"]